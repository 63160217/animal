/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.animal;

/**
 *
 * @author User
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        System.out.println("h1 is animal ? : " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? : " + (h1 instanceof LandAnimal));

        Animal a1 = h1;
        System.out.println("a1 is animal ? : " + (a1 instanceof Animal));
        System.out.println("a1 is reptile animal ? : " + (a1 instanceof Reptile));
        System.out.println();

        Cat c1 = new Cat("Money");
        c1.eat();
        c1.sleep();
        c1.run();
        System.out.println("c1 is animal ? : " + (c1 instanceof Animal));
        System.out.println("c1 is land animal ? : " + (c1 instanceof LandAnimal));

        Animal a2 = c1;
        System.out.println("a2 is animal ? : " + (a2 instanceof Animal));
        System.out.println("a2 is reptile animal ? : " + (a2 instanceof Reptile));
        System.out.println();
        
        Dog d1 = new Dog("Sprite");
        d1.eat();
        d1.sleep();
        d1.run();
        System.out.println("d1 is animal ? : " + (d1 instanceof Animal));
        System.out.println("d1 is land animal ? : " + (d1 instanceof LandAnimal));

        Animal a3 = d1;
        System.out.println("a3 is animal ? : " + (a3 instanceof Animal));
        System.out.println("a3 is aquatic animal ? : " + (a3 instanceof AquaticAnimal));
        System.out.println();
        
        Snake s1 = new Snake("Fanta");
        s1.eat();
        s1.sleep();
        s1.crawl();
        System.out.println("s1 is animal ? : " + (s1 instanceof Animal));
        System.out.println("s1 is reptile animal ? : " + (s1 instanceof Reptile));

        Animal a4 = s1;
        System.out.println("a4 is animal ? : " + (a4 instanceof Animal));
        System.out.println("a4 is aquatic animal ? : " + (a4 instanceof AquaticAnimal));
        System.out.println();
        
        Crocodile cr1 = new Crocodile("Kim");
        cr1.walk();
        cr1.sleep();
        cr1.crawl();
        System.out.println("cr1 is animal ? : " + (cr1 instanceof Animal));
        System.out.println("cr1 is reptile animal ? : " + (cr1 instanceof Reptile));

        Animal a5 = cr1;
        System.out.println("a5 is animal ? : " + (a5 instanceof Animal));
        System.out.println("a5 is poultry animal ? : " + (a5 instanceof Poultry));
        System.out.println();
        
        Fish f1 = new Fish("Nemo");
        f1.eat();
        f1.sleep();
        f1.swim();
        System.out.println("f1 is animal ? : " + (f1 instanceof Animal));
        System.out.println("f1 is aquatic animal ? : " + (f1 instanceof AquaticAnimal));

        Animal a6 = f1;
        System.out.println("a6 is animal ? : " + (a6 instanceof Animal));
        System.out.println("a6 is poultry animal ? : " + (a6 instanceof Poultry));
        System.out.println();
        
        Crab cb1 = new Crab("Garb");
        cb1.walk();
        cb1.sleep();
        cb1.swim();
        System.out.println("cb1 is animal ? : " + (cb1 instanceof Animal));
        System.out.println("cb1 is aquatic animal ? : " + (cb1 instanceof AquaticAnimal));

        Animal a7 = cb1;
        System.out.println("a7 is animal ? : " + (a7 instanceof Animal));
        System.out.println("a7 is land animal ? : " + (a7 instanceof LandAnimal));
        System.out.println();
        
        Bird b1 = new Bird("Kevin");
        b1.walk();
        b1.sleep();
        b1.fly();
        System.out.println("b1 is animal ? : " + (b1 instanceof Animal));
        System.out.println("b1 is poultry animal ? : " + (b1 instanceof Poultry));

        Animal a8 = b1;
        System.out.println("a8 is animal ? : " + (a8 instanceof Animal));
        System.out.println("a8 is land animal ? : " + (a8 instanceof LandAnimal));
        System.out.println();
        
        Bat b2 = new Bat("Justin");
        b2.eat();
        b2.sleep();
        b2.fly();
        System.out.println("b2 is animal ? : " + (b2 instanceof Animal));
        System.out.println("b2 is poultry animal ? : " + (b2 instanceof Poultry));

        Animal a9 = b2;
        System.out.println("a9 is animal ? : " + (a9 instanceof Animal));
        System.out.println("a9 is aquatic animal ? : " + (a9 instanceof AquaticAnimal));
        System.out.println();
        
        
        Plane plane = new Plane("Engine number I");
        Car car = new Car ("Engine number II");
        Flyable[]flyable = {b1,b2,plane};
        for(Flyable f : flyable){
            if (f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        System.out.println("------------------------");
        Runable[]runable = {h1,c1,d1,plane,car};
        for(Runable r : runable){
            r.run();
        }
        
    }
}
